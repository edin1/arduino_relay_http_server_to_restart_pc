/*
  Arduino - Ethernet - HTTP relay control system for restarting PC(s)
  Sources:
  https://forum.ethereum.org/discussion/10809/arduino-to-remotely-reset-a-crashed-miner
  https://www.proto-pic.co.uk/content/arduino/Ether_Relay.zip
  https://www.freetronics.com.au/pages/setting-arduino-ethernet-mac-address
  http://www.instructables.com/id/Controlling-AC-light-using-Arduino-with-relay-modu/
  http://www.hobbyist.co.nz/?q=making-led-lights-dance-to-your-music
*/

#include <SPI.h>
#include <Ethernet.h>
// Enter a MAC address (From the back of the
// shield, or make one up) and IP
// address for your controller below.
byte mac[] = {
  //0x90, 0xA2, 0xDA, 0x0F, 0x64, 0x7B
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
// The IP address will be dependent on your
// local network setup:
IPAddress ip(192,168,1,22);
// Initialize the Ethernet server library
// with the IP address and port you want to
// use (port 80 is default for HTTP):
EthernetServer server(80);
// Initialize a string to hold the control
// string
String readString;
// Set security Mode (0 is not authorised,
// 1 is authorised)
int securityMode = 0;

int RELAY1 = A0;
int RELAY2 = A1;
int LED1 = 13;
int LED2 = 12;
int delayValue = 1000;
int RESET_DELAY = 2000;

void setup()
{
  // Open serial communications for debugging
  //Serial.begin(9600);
  // start the Ethernet connection and the
  // server:
  Ethernet.begin(mac, ip);
  //Ethernet.begin(mac);
  server.begin();
  // Print debug info to the serial monitor
  //Serial.print(F("server is at "));
  //Serial.println(Ethernet.localIP());
  // Set the 4 pins used for the Relay
  // Shield to OUTPUT
  // They go to LOW when you set them as
  // OUTPUT.
  pinMode(RELAY1, OUTPUT);
  pinMode(LED1, OUTPUT);

  pinMode(RELAY2, OUTPUT);
  pinMode(LED2, OUTPUT);

  digitalWrite(RELAY1, HIGH);
  digitalWrite(RELAY2, HIGH);

  digitalWrite(LED1, HIGH);
  digitalWrite(LED2, HIGH);
}
void loop()
{
  EthernetClient client = server.available();
  if (client)
  {
    while (client.connected())
    {
      if (client.available())
      {
        char c = client.read();
        // Maximum length of the passed URL
        // is 100 Characters
        if (readString.length() < 100)
        {
          readString += c;
        }
        // if HTTP request has ended
        if (c == '\n')
        {
          if (securityMode == 0)
          {
            client.println(F("HTTP/1.1 200 OK")); //send new page
            client.println(F("Content-Type: text/html"));
            client.println();
            client.println(F("<HTML>"));
            client.println(F("<HEAD>"));
            client.println(F("<META HTTP-EQUIV=\"CACHE-CONTROL\" CONTENT=\"NO-CACHE\">"));
            client.println(F("<TITLE>Relay Control System</TITLE>")); // Title in browser
            client.println(F("</HEAD>"));
            client.println(F("<BODY>"));
            client.println(F("<H1>Relay control system</H1>")); // Title of page
            client.println(F("<hr />"));
            client.println(F("<br />"));
            // These are not really logged,
            // but printed to the serial
            // monitor
            client.print(F("<p><b> Enter password to access system : </b><br /><i>Warning - Incorrect password entries will be logged.</i></p>"));
            client.print(F("<p> <form method=get>Password: <input type=\"password\" name=\"password\" size=6 autocomplete=\"off\" /> "));
            client.print(F("<input type=\"submit\" value=\"Enter Password\"></form>"));//
            client.println(F("<FORM METHOD=\"LINK\" ACTION=\"http://192.168.1.22\"><INPUT TYPE=\"submit\" VALUE=\"Login\"></FORM>"));
            client.println(F("</BODY>"));
            client.println(F("</HTML>"));
            delay(1);
            client.stop();

            //Serial.println(readString);

            // indexOf returns the location
            // of "monkey" in the readString.
            // if it exists then the password
            // has been entered
            // This will accept
            // \"bigmonkeybanana\" as the
            // password also as it contains
            // the word monkey
            //if(readString.indexOf("monkey") >0) //PASSWORD
	    if (1 == 1)
            {
              {
                //Serial.println(F("Security Set to one."));
                securityMode = 1;
              }
            }
            readString="";
          }
          else
          {
            client.println(F("HTTP/1.1 200 OK")); //send new page
            client.println(F("Content-Type: text/html"));
            client.println();
            client.println(F("<HTML>"));
            client.println(F("<HEAD>"));
            client.println(F("<META HTTP-EQUIV=\"CACHE-CONTROL\" CONTENT=\"NO-CACHE\">"));
            client.println(F("<META HTTP-EQUIV=\"REFRESH\"CONTENT=\"5;URL=http://192.168.1.22\">")); // refresh page every 5 Seconds (So buttons show the correct state.)
            client.println(F("<TITLE>Relay control system</TITLE>")); // Title in browser
            client.println(F("</HEAD>"));
            client.println(F("<BODY>"));
            client.println(F("<H1>Relay control system</H1>")); // Title of page
            client.println(F("<hr />"));
            client.println(F("<br />"));

            /*
            In the following section we
            create the buttons. First we
            read the state of the pin,
            if it is LOW then create the
            button in standard colours,
            if it is HIGH then we create
            the button with a GREEN
            background.
            */
            client.print(F("<input type=button value=\"Relay 1 reset\" onmousedown=location.href=\"/RELAY1RESET\">"));
            client.print(F("<input type=button value=\"Relay 2 reset\" onmousedown=location.href=\"/RELAY2RESET\">"));
            client.print(F("<br>"));
            client.println(F("<input type=button value=\"Log Out\" onmousedown=location.href=\"/LOGOUT\">"));
            client.println(F("</BODY>"));
            client.println(F("</HTML>"));

            delay(1);
            client.stop();
            if(readString.indexOf("RELAY1RESET") > 0)
            {
              //Serial.println(F("Relay 1 reset"));
              digitalWrite(RELAY1, LOW);
	      delay(RESET_DELAY);
              digitalWrite(RELAY1, HIGH);
            }
            if(readString.indexOf("RELAY2RESET") > 0)
            {
              //Serial.println(F("Relay 2 reset"));
              digitalWrite(RELAY2, LOW);
	      delay(RESET_DELAY);
              digitalWrite(RELAY2, HIGH);
            }
            if(readString.indexOf("LOGOUT") >0)// Set security to 0 (disabled)
            {
              //Serial.println(F("Security set to ZERO."));
              securityMode = 0;
            }

            //clearing string for next read
            readString="";
          }
        }
      }
    }
  }
}
